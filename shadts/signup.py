import logging
import gitlab

from flask import request, session, render_template, redirect, url_for

from . import app, make_gitlab_api


logger = logging.getLogger(__name__)


def register_new_user(username, firstname, lastname, email, password):
    gitlab_api = make_gitlab_api()
    
    logger.info("Creating user: {}".format({
        "username": username,
        "firstname": firstname,
        "lastname": lastname,
        "email": email,
    }))
    
    new_user = gitlab_api.users.create({
        "email": email,
        "username": username,
        "name": firstname + " " + lastname,
        "external": False,
        "password": password,
        "confirm": False,
    })
    logger.info("Gitlab user created {}".format(new_user))


def maybe_create_project_for_user(username, user_id):
    gitlab_api = make_gitlab_api()

    course_group = gitlab_api.groups.get(app.config["GITLAB_GROUP"])

    for project in course_group.search('projects', username):
        if project['name'] == username:
            logger.info("Project already exists")
            return
    
    project = gitlab_api.projects.create({
        "name": username,
        "namespace_id": course_group.id,
        "builds_enabled": True,
    })
    logger.info("Git project created {}".format(project))

    member = project.members.create({
        "user_id": user_id,
        "project_id": project.id,
        "access_level": gitlab.MASTER_ACCESS,
    })
    logger.info("Access to project granted {}".format(member))


@app.route("/signup", methods=["GET", "POST"])
def signup():
    if request.method == "GET":
        return render_template("signup.html")

    if request.form["secret"] != app.config["REGISTRATION_SECRET"]:
        return render_template("signup.html", error_message="Invalid secret code")

    try:
        register_new_user(
            request.form["username"],
            request.form["firstname"],
            request.form["lastname"],
            request.form["email"],
            request.form["password"])
    except gitlab.GitlabError as ex:
        logger.error("User registration failed: {}".format(ex.error_message))
        return render_template("signup.html", error_message=ex.error_message)

    return redirect(url_for("login"))
