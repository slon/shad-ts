import re
import json
import gspread

from oauth2client.service_account import ServiceAccountCredentials

from . import app


HEADER_ROW = 2
LOGIN_COLUMN = 3
FLAGS_COLUMN = 4
RATINGS_COLUMN = 8


class LoginNotFound(KeyError):
    pass


class TaskNotFound(KeyError):
    pass


class RatingsTable:
    @classmethod
    def from_config(cls):
        return RatingsTable(
            app.config["GDOC_SPREADSHEET"],
            json.loads(app.config["GDOC_ACCOUNT"]))

    def __init__(self, sheet_id, gdoc_account):
        scope = ['https://spreadsheets.google.com/feeds']    
        credentials = ServiceAccountCredentials.from_json_keyfile_dict(gdoc_account, scope)

        gs = gspread.authorize(credentials)
        self.sheet = gs.open_by_key(sheet_id)
        self.ws = self.sheet.get_worksheet(0)

    def clear(self):
        self.ws.resize(3, 10)
        
        cells = self.ws.range(1, 1, self.ws.row_count, self.ws.col_count)
        updates = []
        for cell in cells:
            if cell.value != "":
                cell.value = ""
                updates.append(cell)

        self.ws.update_cells(updates)

    def list_tasks(self):
        return self.ws.range(HEADER_ROW, RATINGS_COLUMN, HEADER_ROW, self.ws.col_count)

    def find_task_column(self, task):
        all_tasks = self.list_tasks()
        for task_cell in all_tasks:
            if task == task_cell.value:
                return task_cell.col

        raise TaskNotFound("Task '{}' not found in spreadsheet".format(task))

    def sync_task_columns(self, tasks):
        task_cells = self.list_tasks()

        max_size = len(set(tasks) | set(t.value for t in task_cells if t.value != ""))
        if self.ws.col_count < max_size + RATINGS_COLUMN:
            self.ws.resize(cols=RATINGS_COLUMN + max_size)
            task_cells = self.list_tasks()

        added = list(set(tasks) - set(t.value for t in task_cells if t.value != ""))
        tasks = list(filter(lambda x: x in added, tasks))

        for task_cell in task_cells:
            if not tasks:
                break

            if task_cell.value == "":
                task_cell.value = tasks[0]
                tasks.pop(0)

        assert not tasks
        self.ws.update_cells(task_cells)

    def find_login_row(self, login):
        all_logins = self.ws.range(LOGIN_COLUMN, HEADER_ROW+1, self.ws.row_count, LOGIN_COLUMN)

        # Gitlab converts names to lowercase and replaces '.' with '-'
        def to_gitlab(s):
            return s.lower().replace(".", "-")
        
        for login_cell in all_logins:
            if to_gitlab(login) == to_gitlab(login_cell.value):
                return login_cell.row

        raise LoginNotFound("Login {} not found in spreadsheet".format(login))

    def add_login(self, student):
        if len(student["name"]) == 0 or re.match("\W", student["name"], flags=re.UNICODE):
            raise ValueError("Name looks fishy")

        self.ws.append_row([
            '=HYPERLINK("{}";"git")'.format(student["repo"]),
            student["name"],
            student["username"],
        ])

    def put_score(self, student, task, update_fn):
        try:
            student_row = self.find_login_row(student["username"])
        except KeyError:
            self.add_login(student)
            student_row = self.find_login_row(student["username"])

        task_column = self.find_task_column(task)

        flags = self.ws.cell(student_row, FLAGS_COLUMN).value
        score_cell = self.ws.cell(student_row, task_column)
        old_score = int(score_cell.value) if score_cell.value else 0
        score_cell.value = update_fn(flags, old_score)

        self.ws.update_cells([score_cell])
