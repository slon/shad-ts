import copy
import json
import collections
from decimal import Decimal

import flask
import redis

from flask import render_template, session

from . import app
from .login import requires_auth

EXCHANGES = collections.OrderedDict({
    "usd": {"address": "35.197.35.238:27235", "region": "us-west"},
    "crypto": {"address": "35.200.167.175:27235", "region": "asia-south"}
})

CURRENCIES = collections.OrderedDict({
    "BTC-USD": {},
    "ETH-USD": {},
    "ETH-BTC": {},
})

# @app.route("/arbitrage")
@requires_auth
def arbitrage():
    r = redis.StrictRedis(app.config["REDIS"])
    exchanges = copy.deepcopy(EXCHANGES)
    for exchange in exchanges:
        exchanges[exchange]["status"] = r.get("exchange:" + exchange)

    currencies = copy.deepcopy(CURRENCIES)
    for currency in currencies:
        currencies[currency] = json.loads(r.get("currency:" + currency))

    token = r.hget("arbitrage:" + session["gitlab"]["username"], "token")
    if token is not None:
        token = token.decode('utf-8')

    leaderboard = []
    usd_leaderboard = r.hgetall("leaderboard:usd")
    crypto_leaderboard = r.hgetall("leaderboard:crypto")
    for login in (set(usd_leaderboard) & set(crypto_leaderboard)):
        usd_status = json.loads(usd_leaderboard[login])
        crypto_status = json.loads(crypto_leaderboard[login])

        total_packets = usd_status["packets_received"] + crypto_status["packets_received"]
        total_orders = usd_status["orders_executed"] + crypto_status["orders_executed"]
        connected = usd_status["connected"] + crypto_status["connected"]
        if total_packets == 0:
            continue

        score = Decimal(usd_status.get("money", "0")) + Decimal(crypto_status.get("money", "0"))

        entry = {
            "login": login.decode("utf-8"),
            "score": score,
            "packets": total_packets,
            "orders": total_orders,
            "status": {
                0: "offline",
                1: "mixed",
                2: "online",
            }[connected]
        }
        leaderboard.append(entry)

    leaderboard.sort(key=lambda x: x["score"], reverse=True)
    
    return render_template(
        "arbitrage.html",
        exchanges=exchanges,
        leaderboard=leaderboard,
        currencies=currencies,
        token=token)
