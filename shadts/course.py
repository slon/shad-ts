import logging
import pytz
import yaml
import datetime
import requests
import flask
import redis

from flask import session, render_template

from . import app
from .login import requires_auth


logger = logging.getLogger(__name__)
MOSCOW_TIMEZONE = pytz.timezone('Europe/Moscow')


def parse_time(time):
    date = datetime.datetime.strptime(time, "%d-%m-%Y %H:%M")
    return MOSCOW_TIMEZONE.localize(date)


class Task:
    def __init__(self, group, config):
        self.group = group
        self.name = config["task"]
        self.score = config["score"]
        self.tags = config.get("tags", [])
        self.max_attempts = config.get("max_attempts")
        self.url = config.get("url")

    def has_attempts_left(self, attempt):
        if self.max_attempts is not None:
            return attempt < self.max_attempts
        return True

    def is_overdue(self, extra_time=datetime.timedelta()):
        return datetime.datetime.now(MOSCOW_TIMEZONE) > self.group.deadline + extra_time


class Group:
    def __init__(self, config):
        self.name = config["group"]
        self.start = parse_time(config["start"])
        self.deadline = parse_time(config["deadline"])
        self.pretty_deadline = config["deadline"]
        self.tasks = [Task(self, c) for c in config.get("tasks", [])]

    @property
    def is_open(self):
        return datetime.datetime.now(MOSCOW_TIMEZONE) > self.start


class Deadlines:
    def __init__(self, config):
        self.groups = [Group(c) for c in config]

    def find_task(self, name):
        for g in self.groups:
            for task in g.tasks:
                if task.name == name:
                    return task
        raise KeyError("Task {} not found".format(name))

    @property
    def open_groups(self):
        if session["gitlab"].get("is_admin", False):
            return self.groups
        return [g for g in self.groups if g.is_open]

    @property
    def task_names(self):
        names = []
        for g in self.groups:
            for task in g.tasks:
                names.append(task.name)
        return names

    @classmethod
    def fetch(cls):
        deadlines_url = "https://gitlab.com/{}/raw/master/.deadlines.yml".format(
            app.config["COURSE_REPO"])
        raw_rsp = requests.get(deadlines_url)
        raw_rsp.raise_for_status()
        return Deadlines(yaml.safe_load(raw_rsp.content))


@app.route("/")
@requires_auth
def main_page():
    deadlines = Deadlines.fetch()
    solved = redis.StrictRedis(app.config["REDIS"]).hgetall(
        "solved:{}:{}".format(app.config["COURSE_NAME"], session["gitlab"]["username"]))

    return render_template("tasks.html",
       task_base_url="https://gitlab.com/" + app.config["COURSE_REPO"] + "/blob/master",
       deadlines=deadlines,
       solved={k.decode('utf-8') for k in solved})
