FROM python:3

COPY requirements.txt /opt

RUN pip3 install -r /opt/requirements.txt

COPY shadts /opt/shadts

ENV PYTHONPATH=/opt

ENV SHADTS_SETTINGS=/etc/shadts.cfg

EXPOSE 8080

CMD ["gunicorn", "shadts:app", "-b", ":8080", "--worker-class", "gthread", "--threads", "3"]
