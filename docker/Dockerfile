FROM ubuntu:16.04
RUN set -ex && \
    apt-get update -y && \
    apt-get install -y \
        build-essential=12.1ubuntu2 \
        python3.5=3.5.2-2ubuntu0~16.04.4 \
        python3.5-dev=3.5.2-2ubuntu0~16.04.4 \
        python3-pip=8.1.1-2ubuntu0.4 \
        supervisor=3.2.0-2ubuntu0.1 \
        nginx=1.10.3-0ubuntu0.16.04.2 \
        redis-server=2:3.0.6-1

ENV ROOT "/manytask"

WORKDIR ${ROOT}

RUN /usr/bin/pip3 install -q -U pip setuptools
RUN /usr/local/bin/pip3 --version

COPY ./build/requirements.txt ./requirements.txt

RUN /usr/local/bin/pip3 install -U -I -r ./requirements.txt --no-cache-dir

RUN mkdir -p /var/log/supervisor
RUN mkdir -p /var/log/gunicorn
RUN mkdir -p /manytask/persistent-disk
COPY ./supervisord.conf /etc/supervisor/conf.d/supervisord.conf

COPY nginx.conf /etc/nginx/nginx.conf

COPY redis.conf /etc/redis/redis.conf

COPY ./build .
RUN /usr/local/bin/pip3 install -v .

CMD /usr/bin/supervisord

EXPOSE 80
