PROJECT:=test-shad
CONTAINER_NAME:=Manytask
INSTANCE_NAME:=instance-web
IP:=localhost:80
DOCKER_CONFIG_NAME:=local.cfg

ifeq ($(PROD),1)
	DOCKER_BASE:=gcr.io/$(PROJECT)/web
	ENV_FILE:=../env-prod.cfg
	ENV_VARIABLES:=$(shell cat ../env-prod.cfg)
else
	DOCKER_BASE:=gcr.io/$(PROJECT)/web-test
	ENV_FILE:=../env-local.cfg
	ENV_VARIABLES:=$(shell cat ../env-local.cfg)
endif

DOCKER_URL_VERSION:=1.$(shell date +%s)
DOCKER_URL:=$(DOCKER_BASE):$(DOCKER_URL_VERSION)


OS := $(shell uname)

build:
	mkdir -p ./build
	cp -RL ../setup.py ./build
	cp -RL ../requirements.txt ./build
	cp -RL ../shadts ./build
	cp -RL $(ENV_FILE) ./build/$(DOCKER_CONFIG_NAME)

clean:
	rm -rf ./build

docker: clean build
	echo "Building $(DOCKER_URL)"
	docker build -t $(DOCKER_URL) .

volume:
	docker volume create redis-volume

run: volume docker
	-docker rm $(CONTAINER_NAME) -f
	docker run -p 5000:80 -p 443:443 \
			--name $(CONTAINER_NAME) \
			--env SHADTS_SETTINGS=/manytask/$(DOCKER_CONFIG_NAME) \
			--mount type=volume,source=redis-volume,target=/manytask/persistent-disk \
			$(DOCKER_URL)

push: docker
	gcloud docker -- push $(DOCKER_URL)

login:
	docker exec -it $(shell docker ps -f name=$(CONTAINER_NAME) -q) /bin/bash

logs:
	docker exec -it $(shell docker ps -f name=$(CONTAINER_NAME) -q) /bin/bash -c 'tail -f /var/log/supervisor/*'

###################
# In develop
###################

install-local:
ifeq ($(OS),Darwin)
	pip3 install -U -I -r ../requirements.txt
	brew install redis
else
	echo "There are no rules for your OS"
endif


run-redis-local:
ifeq ($(OS),Darwin)
	-redis-cli shutdown
	redis-server ./redis.conf
else
	echo "There are no rules for your OS"
endif


run-dev: run-redis-dev
	env $(ENV_VARIABLES) python3 ../web.py


submit:
	curl -X POST $(IP)/submit -F 'flag=1'


config-project:
	gcloud auth login
	gcloud config set project $(PROJECT)


config-firewall:
	gcloud compute firewall-rules create allow-http --allow tcp:80 --target-tags http-server


create: docker
	gcloud beta compute instances create-with-container $(INSTANCE_NAME)  \
		--container-image $(DOCKER_URL) \
		--tags http-server,https-server \
		--zone=europe-west1-b \
		--machine-type=f1-micro  \
		--container-env-file $(ENV_FILE) \
		--container-mount-host-path host-path=/mnt/disks,mount-path=/manytask/persistent-disk

deploy: docker
	gcloud beta compute instances update-container $(INSTANCE_NAME)  \
		--container-image $(DOCKER_URL)

ssh:
	gcloud beta compute ssh $(INSTANCE_NAME) --zone europe-west1-b
