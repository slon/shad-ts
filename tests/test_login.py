import pytest
import requests_mock
import urllib

from flask import session

from .test_app import test_app


def test_login(test_app):
    with test_app.test_client() as client:
        rsp = client.get("/login")
        assert rsp.status_code == 302

        redirect = urllib.parse.urlparse(rsp.headers["Location"])
        assert redirect.path == "/oauth/authorize"
        params = urllib.parse.parse_qs(redirect.query)
        assert params["redirect_uri"] == ["http://localhost/login_finish"]


def test_login_finish(test_app):
    with test_app.test_client() as client:
        with client.session_transaction() as session:
            session["oauth_state"] = "2"

        with requests_mock.mock() as m:
            m.post("https://gitlab.manytask.org/oauth/token", json={"access_token": "123"})
            m.get("https://gitlab.manytask.org/api/v4/user", json={
                "username": "prime", "token": "567", "id": "1", "is_admin": True
            })
            
            rsp = client.get("/login_finish?code=1&state=2&nocreate=1")

        with client.session_transaction() as session:
            assert session["gitlab"]["username"] == "prime"

def test_logout(test_app):
    with test_app.test_client() as client:
        with client.session_transaction() as session:
            session["gitlab"] = {"username": "prime"}

        client.get("/logout")

        with client.session_transaction() as session:
            assert "gitlab" not in session

